package br.com.fiap.saude.repository;

import br.com.fiap.saude.model.Vendedor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VendedorRepository extends JpaRepository<Vendedor, Integer> {
    List<Vendedor> findByNome(String nome);
}
