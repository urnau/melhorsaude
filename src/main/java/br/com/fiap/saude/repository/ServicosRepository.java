package br.com.fiap.saude.repository;

import br.com.fiap.saude.model.Servicos;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ServicosRepository extends JpaRepository<Servicos, Integer> {
    List<Servicos> findByNome(String nome);
}
