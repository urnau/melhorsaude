package br.com.fiap.saude.repository;

import br.com.fiap.saude.model.Agendamento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AgendamentoRepository extends JpaRepository<Agendamento, Integer> {
    List<Agendamento> findByContato(String contato);
}
