package br.com.fiap.saude.repository;

import br.com.fiap.saude.model.Clinica;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClinicaRepository extends JpaRepository<Clinica, Integer> {
    List<Clinica> findByNome(String nome);
}
