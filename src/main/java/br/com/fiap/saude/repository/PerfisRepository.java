package br.com.fiap.saude.repository;

import br.com.fiap.saude.model.Clinica;
import br.com.fiap.saude.model.Perfis;
import br.com.fiap.saude.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PerfisRepository extends JpaRepository<br.com.fiap.saude.model.Perfis, Integer> {
    List<Perfis> findByClinica(Clinica clinica);
    Usuario findByUsuario(Usuario usuario);

    boolean existsByUsuario(Usuario usuario);
}
