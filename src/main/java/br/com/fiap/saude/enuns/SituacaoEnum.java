package br.com.fiap.saude.enuns;

public enum SituacaoEnum {
    ATIVO,
    PAUSADO,
    INATIVO,
    REMOVIDO,
    BLOQUEADO
}
