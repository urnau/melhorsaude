package br.com.fiap.saude.enuns;

public enum PermissaoEnum {
    ADMIN,
    NORMAL,
    OPERADOR
}