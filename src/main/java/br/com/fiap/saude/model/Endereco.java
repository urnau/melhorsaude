package br.com.fiap.saude.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@SequenceGenerator(name="endereco", sequenceName = "SQ_ENDERECO", allocationSize = 1)
public class Endereco {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "endereco")
    private Long id;

    @NotBlank(message = "Lougadouro do endereco obrigatório!")
    @Size(max=10)
    private String lougradouro;

    @NotBlank(message = "Nome do endereço é obrigatório!")
    @Size(max=60)
    private String nome;

    @NotBlank(message = "Número é obrigátorio!")
    @Size(max=8)
    private String numero;

    @Size(max = 20)
    private String complemento;

    private int cep;

    @Size(max = 15)
    private String telefone;

    @Size(max = 20)
    private String referencia;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLougradouro() {
        return lougradouro;
    }

    public void setLougradouro(String lougradouro) {
        this.lougradouro = lougradouro;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public int getCep() {
        return cep;
    }

    public void setCep(int cep) {
        this.cep = cep;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
}
