package br.com.fiap.saude.model;

import br.com.fiap.saude.enuns.PermissaoEnum;
import br.com.fiap.saude.enuns.SituacaoEnum;
import com.fasterxml.jackson.annotation.*;


import javax.persistence.*;
import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@SequenceGenerator(name="perfis", sequenceName = "SQ_PERFIS", allocationSize = 1)
public class Perfis {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "perfis")
    private int id;

    private SituacaoEnum situacaoUsuario;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_usuario_id", unique = true)

    private Usuario usuario;


    /* Cliente */
    private PermissaoEnum permissaoCliente;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_cliente_id", unique = true, nullable = true)

    private Cliente cliente;


    /* Vendedor */
    private PermissaoEnum permissaoVendedor;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_vendedor_id", unique = false, nullable = true)

    private Vendedor vendedor;


    /* Clinica */
    private PermissaoEnum permissaoClinica;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_clinica_id", unique = false, nullable = true)

    private Clinica clinica;


    /* Especialista */
    private PermissaoEnum permissaoEspecialista;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_especialista_id", unique = false, nullable = true)

    private Especialista especialista;

//    @OneToMany(mappedBy = "id", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    public List<Vendedor> vendedor;

    public Perfis() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SituacaoEnum getSituacaoUsuario() {
        return situacaoUsuario;
    }

    public void setSituacaoUsuario(SituacaoEnum situacaoUsuario) {
        this.situacaoUsuario = situacaoUsuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public PermissaoEnum getPermissaoCliente() {
        return permissaoCliente;
    }

    public void setPermissaoCliente(PermissaoEnum permissaoCliente) {
        this.permissaoCliente = permissaoCliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public PermissaoEnum getPermissaoVendedor() {
        return permissaoVendedor;
    }

    public void setPermissaoVendedor(PermissaoEnum permissaoVendedor) {
        this.permissaoVendedor = permissaoVendedor;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public PermissaoEnum getPermissaoClinica() {
        return permissaoClinica;
    }

    public void setPermissaoClinica(PermissaoEnum permissaoClinica) {
        this.permissaoClinica = permissaoClinica;
    }

    public Clinica getClinica() {
        return clinica;
    }

    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

    public PermissaoEnum getPermissaoEspecialista() {
        return permissaoEspecialista;
    }

    public void setPermissaoEspecialista(PermissaoEnum permissaoEspecialista) {
        this.permissaoEspecialista = permissaoEspecialista;
    }

    public Especialista getEspecialista() {
        return especialista;
    }

    public void setEspecialista(Especialista especialista) {
        this.especialista = especialista;
    }



}
