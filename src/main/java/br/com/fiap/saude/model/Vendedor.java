package br.com.fiap.saude.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@SequenceGenerator(name="vendedor", sequenceName = "SQ_VENDEDOR", allocationSize = 1)
public class Vendedor {

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "vendedor")
    private int id;

    @NotBlank(message = "Nome obrigatório!")
    private String nome;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
