package br.com.fiap.saude.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@SequenceGenerator(name="especialista", sequenceName = "SQ_ESPECIALISTA", allocationSize = 1)
public class Especialista {

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "especialista")
    private int id;

    @Size(max = 45)
    private String graduacao;

    @Size(max = 60)
    private String especializacao;

    @NotBlank(message = "Nome obrigatório")
    @Size(max = 100)
    private String nome;

    @NotBlank(message = "CRM é obrigatório")
    @Size(max=45)
    private String crm;

    @Size(max = 300)
    private String descricao;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "especialista")
    private List<Servicos> servicos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGraduacao() {
        return graduacao;
    }

    public void setGraduacao(String graduacao) {
        this.graduacao = graduacao;
    }

    public String getEspecializacao() {
        return especializacao;
    }

    public void setEspecializacao(String especializacao) {
        this.especializacao = especializacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCrm() {
        return crm;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Servicos> getServicos() {
        return servicos;
    }

    public void setServicos(List<Servicos> servicos) {
        this.servicos = servicos;
    }
}
