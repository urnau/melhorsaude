package br.com.fiap.saude.controller;

import br.com.fiap.saude.model.Usuario;
import br.com.fiap.saude.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("usuario")
public class UsuarioController {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @GetMapping("cadastrar")
    public String abrirFormulario(Usuario usuario) {
        return "usuario/form";
    }

    @PostMapping("cadastrar")
    public String processarForm(Usuario usuario, BindingResult result, RedirectAttributes redirectAttributes){
        if(result.hasErrors()){
            return "usuario/form";
        }
        redirectAttributes.addFlashAttribute("msg","Cadastrado!");
            usuarioRepository.save(usuario);
        return "redirect:listar";
    }

    @GetMapping("listar")
    public String listarUsuarios(Model model){
        model.addAttribute("usuario",usuarioRepository.findAll());
        return "usuario/lista";
    }

    @GetMapping("editar/{id}")
    public String editar(@PathVariable("id") int id, Model model){
        model.addAttribute("usuario", usuarioRepository.findById(id));
        return "usuario/form";
    }

    @PostMapping("excluir")
    public String remover(int id, RedirectAttributes redirectAttributes){
        redirectAttributes.addFlashAttribute("msg","Removido!");
        usuarioRepository.deleteById(id);
        return "redirect:listar";
    }


}
