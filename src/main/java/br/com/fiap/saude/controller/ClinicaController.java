package br.com.fiap.saude.controller;

import br.com.fiap.saude.model.Clinica;
import br.com.fiap.saude.repository.ClinicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("clinica")
public class ClinicaController {

    @Autowired
    private ClinicaRepository clinicaRepository;

    @GetMapping("cadastrar")
    public String abrirFormulario(Clinica clinica) {
        return "clinica/form";
    }


    @PostMapping("cadastrar")
    public String processarForm(Clinica clinica, BindingResult result, RedirectAttributes redirectAttributes){
        if(result.hasErrors()){
            return "clinica/form";
        }
        redirectAttributes.addFlashAttribute("msg","Cadastrado!");
            clinicaRepository.save(clinica);
        return "redirect:listar";
    }

    @GetMapping("listar")
    public String listarClinicas(Model model){
        model.addAttribute("clinica",clinicaRepository.findAll());
        return "clinica/lista";
    }

    @GetMapping("editar/{id}")
    public String editar(@PathVariable("id") int id, Model model){
        model.addAttribute("clinica", clinicaRepository.findById(id));
        return "clinica/form";
    }

    @PostMapping("excluir")
    public String remover(int id, RedirectAttributes redirectAttributes){
        redirectAttributes.addFlashAttribute("msg","Removido!");
        clinicaRepository.deleteById(id);
        return "redirect:listar";
    }


}
