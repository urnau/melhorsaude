package br.com.fiap.saude.controller;

import br.com.fiap.saude.model.Cliente;
import br.com.fiap.saude.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("cliente")
public class ClienteController {

    @Autowired
    private ClienteRepository clienteRepository;

    @GetMapping("cadastrar")
    public String abrirFormulario(Cliente cliente) {
        return "cliente/form";
    }


    @PostMapping("cadastrar")
    public String processarForm(Cliente cliente, BindingResult result, RedirectAttributes redirectAttributes){
        if(result.hasErrors()){
            return "cliente/form";
        }
        redirectAttributes.addFlashAttribute("msg","Cadastrado!");
            clienteRepository.save(cliente);
        return "redirect:listar";
    }

    @GetMapping("listar")
    public String listarClientes(Model model){
        model.addAttribute("cliente",clienteRepository.findAll());
        return "cliente/lista";
    }

    @GetMapping("editar/{id}")
    public String editar(@PathVariable("id") int id, Model model){
        model.addAttribute("cliente", clienteRepository.findById(id));
        return "cliente/form";
    }

    @PostMapping("excluir")
    public String remover(int id, RedirectAttributes redirectAttributes){
        redirectAttributes.addFlashAttribute("msg","Removido!");
        clienteRepository.deleteById(id);
        return "redirect:listar";
    }


}
