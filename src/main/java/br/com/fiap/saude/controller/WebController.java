package br.com.fiap.saude.controller;

import br.com.fiap.saude.model.Usuario;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class WebController {
    @GetMapping("/")
    public String homePage() {
        return "web/index";
    }

}
