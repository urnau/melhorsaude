package br.com.fiap.saude.controller;

import br.com.fiap.saude.model.Perfis;
import br.com.fiap.saude.model.Usuario;
import br.com.fiap.saude.repository.PerfisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("perfis")
public class PerfisController {

    @Autowired
    private PerfisRepository perfisRepository;

    @GetMapping("cadastrar")
    public String abrirFormulario(Perfis perfis) {
        return "perfis/form";
    }

    @PostMapping("cadastrar")
    public String processarForm(Perfis perfis, BindingResult result, RedirectAttributes redirectAttributes){
        if(result.hasErrors()){
            return "perfis/form";
        }
        redirectAttributes.addFlashAttribute("msg","Cadastrado!");
        perfisRepository.save(perfis);
        return "redirect:listar";

    }

    @GetMapping("listar")
    public String listarPerfiss(Model model){
        model.addAttribute("perfis",perfisRepository.findAll());
        return "perfis/lista";
    }

    @GetMapping("editar/{id}")
    public String editar(@PathVariable("id") int id, Model model){
        model.addAttribute("perfis", perfisRepository.findById(id));
        return "perfis/form";
    }

    @PostMapping("excluir")
    public String remover(int id, RedirectAttributes redirectAttributes){
        redirectAttributes.addFlashAttribute("msg","Removido!");
        perfisRepository.deleteById(id);
        return "redirect:listar";
    }

}
