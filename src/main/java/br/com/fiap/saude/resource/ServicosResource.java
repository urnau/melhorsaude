package br.com.fiap.saude.resource;

import br.com.fiap.saude.model.Clinica;
import br.com.fiap.saude.model.Perfis;
import br.com.fiap.saude.model.Servicos;
import br.com.fiap.saude.model.Usuario;
import br.com.fiap.saude.repository.ClinicaRepository;
import br.com.fiap.saude.repository.ServicosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("servicos")
public class ServicosResource {

    @Autowired
    private ServicosRepository servicosRepository;

    @Autowired
    private ClinicaRepository clinicaRepository;

    @GetMapping
    public List<Servicos> listar() {
        return servicosRepository.findAll();
    }

    @GetMapping("{id}")
    public Servicos buscar(@PathVariable int id) {
        return servicosRepository.findById(id).get();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Servicos cadastrar(@RequestBody Servicos servicos) {
        try{
            return servicosRepository.save(servicos);
        }catch ( Exception e){
            return servicos;
        }
    }

    @PutMapping("{id}")
    public Servicos atualizar(@RequestBody Servicos servicos, @PathVariable int id) {
        servicos.setId(id);
        return servicosRepository.save(servicos);
    }

    @DeleteMapping("{id}")
    public void remover(@PathVariable int id) {
        servicosRepository.deleteById(id);
    }


    /**
     * @apiNote Cadatrar Perfil e associa com um Usuário
     * */
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("clinica/{clinica_id}")
    public Servicos cadastrarComClinica(@RequestBody Servicos servicos, @PathVariable int clinica_id) {
        try {
            Clinica clinica = clinicaRepository.getOne(clinica_id);
            servicos.setClinica(clinica);
            return servicosRepository.save(servicos);
        }catch (Exception e){
            return null;
        }
    }

    @PutMapping("clinica/{id}/{clinica_id}")
    public Servicos atualizarClinica(@RequestBody Servicos servicos, @PathVariable int id, @PathVariable int clinica_id) {
        try {
            Servicos modelo = servicosRepository.getOne(id);
            modelo.setClinica(clinicaRepository.getOne(clinica_id));
            return servicosRepository.save(modelo);
        }catch (Exception e){
            return null;
        }
    }



}
