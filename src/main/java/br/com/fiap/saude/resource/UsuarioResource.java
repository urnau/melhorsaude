package br.com.fiap.saude.resource;

import br.com.fiap.saude.model.Usuario;
import br.com.fiap.saude.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("usuario")
public class UsuarioResource {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @GetMapping
    public List<Usuario> listar() {
        return usuarioRepository.findAll();
    }

    @GetMapping("{id}")
    public Usuario buscar(@PathVariable int id) {
        return usuarioRepository.findById(id).get();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Usuario cadastrar(@RequestBody Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    @PutMapping("{id}")
    public Usuario atualizar(@RequestBody Usuario usuario, @PathVariable int id) {
        usuario.setId(id);
        return usuarioRepository.save(usuario);
    }

    @DeleteMapping("{id}")
    public void remover(@PathVariable int id) {
        usuarioRepository.deleteById(id);
    }

}
