package br.com.fiap.saude.resource;

import br.com.fiap.saude.model.*;
import br.com.fiap.saude.repository.ClienteRepository;
import br.com.fiap.saude.repository.ClinicaRepository;
import br.com.fiap.saude.repository.PerfisRepository;
import br.com.fiap.saude.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("perfis")
public class PerfisResource {

    @Autowired
    private PerfisRepository perfisRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private ClinicaRepository clinicaRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @GetMapping("modelo")
    public Perfis modelo() {
        return new Perfis();
    }

    @GetMapping
    public List<Perfis> listar() {
        return perfisRepository.findAll();
    }

    @GetMapping("{id}")
    public Perfis buscar(@PathVariable int id) {
        return perfisRepository.findById(id).get();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Perfis cadastrar(@RequestBody Perfis perfis) {
        try {
           return perfisRepository.save(perfis);
        }catch (Exception e){
            return null;
        }
    }

    @PutMapping("{id}")
    public Perfis atualizar(@RequestBody Perfis perfis, @PathVariable int id) {
        try {
            perfis.setId(id);
            Perfis modelo = perfisRepository.getOne(id);
            if(!usuarioRepository.existsById( perfis.getUsuario().getId() ))
                perfis.setUsuario(modelo.getUsuario());

            return perfisRepository.save(perfis);
        }catch (Exception e){
            return null;
        }
    }

    @DeleteMapping("{id}")
    public void remover(@PathVariable int id) {
        perfisRepository.deleteById(id);
    }

    /**
     * @apiNote Cadatrar Perfil e associa com uma Clinica
     * */
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("clinica/{clinica_id}")
    public Perfis cadastrarComClinica(@RequestBody Perfis perfis, @PathVariable int clinica_id) {
        try {
            Clinica clinica = clinicaRepository.getOne(clinica_id);
            perfis.setClinica(clinica);
            return perfisRepository.save(perfis);
        }catch (Exception e){
            return null;
        }
    }

    @PutMapping("clinica/{id}/{clinica_id}")
    public Perfis atualizarClinica(@RequestBody Perfis perfis, @PathVariable int id, @PathVariable int clinica_id) {
        try {
            Perfis _perfis = perfisRepository.getOne(id);
            _perfis.setClinica(clinicaRepository.getOne(clinica_id));
//            _perfis.setPermissaoClinica(perfis.getPermissaoClinica());
            return perfisRepository.save(_perfis);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * @apiNote Cadatrar Perfil e associa com um Usuário
     * */
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("usuario/{usuario_id}")
    public Perfis cadastrarComUsuario(@RequestBody Perfis perfis, @PathVariable int usuario_id) {
        try {
            Usuario usuario = usuarioRepository.getOne(usuario_id);
            perfis.setUsuario(usuario);
            return perfisRepository.save(perfis);
        }catch (Exception e){
            return null;
        }
    }


    @PutMapping("usuario/{id}/{usuario_id}")
    public Perfis atualizarUsuario(@RequestBody Perfis perfis, @PathVariable int id, @PathVariable int usuario_id) {
        try {
            Perfis modelo = perfisRepository.getOne(id);
            modelo.setUsuario(usuarioRepository.getOne(usuario_id));
            return perfisRepository.save(modelo);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * @apiNote Cadatrar Perfil e associa com um Cliente
     * */
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("cliente/{cliente_id}")
    public Perfis cadastrarComCliente(@RequestBody Perfis perfis, @PathVariable int cliente_id) {
        try {
            Cliente cliente = clienteRepository.getOne(cliente_id);
            perfis.setCliente(cliente);
            return perfisRepository.save(perfis);
        }catch (Exception e){
            return null;
        }
    }

    @PutMapping("cliente/{id}/{cliente_id}")
    public Perfis atualizarCliente(@RequestBody Perfis perfis, @PathVariable int id, @PathVariable int cliente_id) {
        try {
            Perfis modelo = perfisRepository.getOne(id);
            modelo.setCliente(clienteRepository.getOne(cliente_id));
//            modelo.setPermissaoCliente(perfis.getPermissaoCliente());
            return perfisRepository.save(modelo);
        }catch (Exception e){
            return null;
        }
    }



}
