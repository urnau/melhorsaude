package br.com.fiap.saude.resource;

import br.com.fiap.saude.model.Vendedor;
import br.com.fiap.saude.repository.VendedorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("vendedor")
public class VendedorResource {

    @Autowired
    private VendedorRepository vendedorRepository;

    @GetMapping
    public List<Vendedor> listar() {
        return vendedorRepository.findAll();
    }

    @GetMapping("{id}")
    public Vendedor buscar(@PathVariable int id) {
        return vendedorRepository.findById(id).get();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Vendedor cadastrar(@RequestBody Vendedor vendedor) {
        return vendedorRepository.save(vendedor);
    }

    @PutMapping("{id}")
    public Vendedor atualizar(@RequestBody Vendedor vendedor, @PathVariable int id) {
        vendedor.setId(id);
        return vendedorRepository.save(vendedor);
    }

    @DeleteMapping("{id}")
    public void remover(@PathVariable int id) {
        vendedorRepository.deleteById(id);
    }

}
