package br.com.fiap.saude.resource;

import br.com.fiap.saude.model.Cliente;
import br.com.fiap.saude.model.Clinica;
import br.com.fiap.saude.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("cliente")
public class ClienteResource {

    @Autowired
    private ClienteRepository clienteRepository;

    @GetMapping("modelo")
    public Cliente modelo() {
        return new Cliente();
    }

    @GetMapping
    public List<Cliente> listar() {
        return clienteRepository.findAll();
    }

    @GetMapping("{id}")
    public Cliente buscar(@PathVariable int id) {
        return clienteRepository.findById(id).get();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Cliente cadastrar(@RequestBody Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    @PutMapping("{id}")
    public Cliente atualizar(@RequestBody Cliente cliente, @PathVariable int id) {
        cliente.setId(id);
        return clienteRepository.save(cliente);
    }

    @DeleteMapping("{id}")
    public void remover(@PathVariable int id) {
        clienteRepository.deleteById(id);
    }

}
