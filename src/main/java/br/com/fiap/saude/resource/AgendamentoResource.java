package br.com.fiap.saude.resource;

import br.com.fiap.saude.model.Agendamento;
import br.com.fiap.saude.repository.AgendamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("agendamento")
public class AgendamentoResource {

    @Autowired
    private AgendamentoRepository agendamentoRepository;

    @GetMapping("modelo")
    public Agendamento modelo() {
        return new Agendamento();
    }

    @GetMapping
    public List<Agendamento> listar() {
        return agendamentoRepository.findAll();
    }

    @GetMapping("{id}")
    public Agendamento buscar(@PathVariable int id) {
        return agendamentoRepository.findById(id).get();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Agendamento cadastrar(@RequestBody Agendamento agendamento) {
        return agendamentoRepository.save(agendamento);
    }

    @PutMapping("{id}")
    public Agendamento atualizar(@RequestBody Agendamento agendamento, @PathVariable long id) {
        agendamento.setId(id);
        return agendamentoRepository.save(agendamento);
    }

    @DeleteMapping("{id}")
    public void remover(@PathVariable int id) {
        agendamentoRepository.deleteById(id);
    }

}
