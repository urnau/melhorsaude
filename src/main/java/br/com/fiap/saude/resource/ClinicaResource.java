package br.com.fiap.saude.resource;

import br.com.fiap.saude.model.Clinica;
import br.com.fiap.saude.repository.ClinicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("clinica")
public class ClinicaResource {

    @Autowired
    private ClinicaRepository clinicaRepository;

    @GetMapping
    public List<Clinica> listar() {
        return clinicaRepository.findAll();
    }

    @GetMapping("{id}")
    public Clinica buscar(@PathVariable int id) {
        return clinicaRepository.findById(id).get();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Clinica cadastrar(@RequestBody Clinica clinica) {
        return clinicaRepository.save(clinica);
    }

    @PutMapping("{id}")
    public Clinica atualizar(@RequestBody Clinica clinica, @PathVariable int id) {
        clinica.setId(id);
        return clinicaRepository.save(clinica);
    }

    @DeleteMapping("{id}")
    public void remover(@PathVariable int id) {
        clinicaRepository.deleteById(id);
    }

}
